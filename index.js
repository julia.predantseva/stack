console.log('hi');
class Stack {
    #length;
    #data;
    constructor(){
        this.#length = 0 || arguments.length;
        this.#data = {...arguments};
    }
    push = (arg) => {
        if(!arg){
            throw new SyntaxError('no arguments');
        }
        let newLength = ++this.#length;
        this.#data[newLength] = arg;
    }
    pop = () => { 
        if(!this.#length){
            throw new SyntaxError('stack is empty');
        }
        let newLength = this.#length
        let deletedData = this.#data[newLength];
        delete this.#data[newLength];
        --this.#length;
        return deletedData
    }

    peek = () => {
        return this.#data[this.#length]
    }
    count =() => {
        return this.#length;
    }
};



const stack = new Stack();
stack.push('fdf')
stack.push('qqqqqqqqq')
stack.push(1213)
console.log(stack)

let a = new Stack('fdsf', 'csdvcsfvfd', 67)